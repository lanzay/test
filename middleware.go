package main

import (
	"crypto/md5"
	"fmt"
	"log"
	"net"
	"net/http"
	"net/http/httputil"
	"os"
	"time"
)

const COOKIE_NAME = "x"

type (
	Server struct {
		cfg Config
	}
	Config struct {
		Addr string
		Port int
	}
)

func main() {
	c := NewConfigDefault()
	s := NewServer(c)
	s.Run()
}

func NewConfigDefault() Config {
	return Config{
		Addr: "",
		Port: 81,
	}
}

func NewServer(c Config) Server {
	return Server{
		cfg: c,
	}
}

func (s *Server) Run() {

	mux := http.NewServeMux()
	mux.HandleFunc("/", logWww(wwwIndex))

	addr := fmt.Sprintf("%s:%d", s.cfg.Addr, s.cfg.Port)
	log.Println("[F] Server.Run", http.ListenAndServe(addr, mux))
}

func logWww(f http.HandlerFunc) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		//==== MW logic - logging, authentification ...
		log.Printf("[D] Remote: %s, Host: %s, URI: %s\n", r.RemoteAddr, r.Host, r.RequestURI)

		ok := checkCookie(r)
		if !ok {
			host, _, _ := net.SplitHostPort(r.Host)
			setCookie(&w, host)
			//wwwLogin()
		}
		//====

		f(w, r)
	}
}

func setCookie(w *http.ResponseWriter, host string) {

	c := http.Cookie{
		Name:    COOKIE_NAME,
		Value:   fmt.Sprintf("%x", md5.Sum(time.Now().AppendFormat([]byte("salt"), time.RFC3339Nano))),
		Path:    "/",
		Domain:  host,
		Expires: time.Now().AddDate(0, 0, 1),
	}
	http.SetCookie(*w, &c)

}

func checkCookie(r *http.Request) bool {

	c, _ := r.Cookie(COOKIE_NAME)
	if c != nil {
		//Check session -> DB...
		log.Println("[I] SSID:", c.Value)
		return true
	}
	return false
}

func wwwIndex(w http.ResponseWriter, r *http.Request) {

	dumpR, err := httputil.DumpRequest(r, true)
	if err != nil {
		log.Panicln("[F] wwwIndex ", err)
	}

	if os.Getenv("ENV") == "DEB" {
		fmt.Println(string(dumpR))
	}
	_, err = w.Write(dumpR)
	log.Println("[W] wwwIndex Write", err)

}
